package ru.qdts.xtooc.model.mixture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ListIterator;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class CompositionTest {
	static Composition comp = null;
	
	@Test
	@Order(1)
	void createComposition() {
		double [] dArray = {0.2, 0.3, 0.5};
		try {
			comp = Composition.of(dArray);
		} catch (UnnormolizedCompositionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(comp);
	}
	
	@Test
	@Order(2)
	void iterateTest() {
		double s = 0;
		for(Double val : comp) {
			s += val;
		}
		assertEquals(1.0, s);
	}
	
	@Test
	@Order(3)
	void getTest() {
		assertEquals(0.2, comp.get(0));
		assertEquals(0.3, comp.get(1));
		assertEquals(0.5, comp.get(2));
	}
	
	@Test
	@Order(4)
	void unmodifiebleTest() {
		ListIterator<Double> i = comp.iterator();
		assertThrows(UnsupportedOperationException.class, () -> i.add(0.9));
		assertThrows(UnsupportedOperationException.class, () -> i.set(0.8));
	}
	
	@Test
	void createUnnormolizedComposition() {
		double [] dArray = {0.2, 0.3, 0.4};
		assertThrows(UnnormolizedCompositionException.class, () -> Composition.of(dArray));
	}
	
}
