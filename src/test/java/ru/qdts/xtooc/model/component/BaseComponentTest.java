package ru.qdts.xtooc.model.component;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ru.qdts.xtooc.model.component.BaseComponent.BoilingTemperatureCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.DensityCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.EnthalpyOfLiquidCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.EnthalpyOfVaporCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.EnthalpyOfVaporizationCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.HeatCapacityOfVaporCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.PressureCalculator;
import ru.qdts.xtooc.model.component.BaseComponent.ViscosityCalculator;

public class BaseComponentTest {
	
	static final String NAME = "ComponentName";
	static final double MOLEC_MASS = 1.5;
	static final double TEMP_CRIT = 12.5;
	static final double ST_ENTHALPY = 123.5;
	static final double ST_ENTHALPY_OF_VAPORIZATION = 1234.5;
	
	static BaseComponent bc = null;
	
	@BeforeAll
	static void createComponent() {
		
		bc = new BaseComponent(NAME, MOLEC_MASS, TEMP_CRIT, ST_ENTHALPY, ST_ENTHALPY_OF_VAPORIZATION,
				new DensityCalculatorImpl(1,2,3,4),
				new ViscosityCalculatorImpl(1,2),
				null, null, null, null, null, null);
//				EnthalpyOfVaporizationCalculator enthOfVzationCalc, BoilingTemperatureCalculator boilTempCalc,
//				PressureCalculator pressureCalc, HeatCapacityOfVaporCalculator heatCapOfVCalc,
//				EnthalpyOfVaporCalculator enthOfVCalc, EnthalpyOfLiquidCalculator enthOfLCalc)
	}
	
	@Test
	void getNameTest() {
		assertTrue(NAME.equals(bc.getName()));
	}
	
	@Test
	void getMolecMassTest() {
		assertEquals(MOLEC_MASS, bc.getMolecularMass());
	}
	
	@Test
	void getTempCriticalTest() {
		assertEquals(TEMP_CRIT, bc.getTempCritical());
	}
	
	@Test
	void getStEnthalpyTest() {
		assertEquals(ST_ENTHALPY, bc.getStEnthalpy());
	}
	
	@Test
	void getStEnthalpyOfVaporizTest() {
		assertEquals(ST_ENTHALPY_OF_VAPORIZATION, bc.getStEnthalpyOfVaporiz());
	}
}
