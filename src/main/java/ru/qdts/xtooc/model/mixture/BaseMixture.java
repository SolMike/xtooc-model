package ru.qdts.xtooc.model.mixture;

import java.util.List;

import ru.qdts.xtooc.model.component.Component;

public class BaseMixture implements Mixture {
	
	private List<Component> components;
	private Composition composition;
	
	private BaseMixture(List<Component> lc, Composition c) {
		components = List.copyOf(lc);
		composition = c;
	}
	
	public static Mixture of(List<Component> compList, Composition compos) throws MixtureException {
		if(compList == null || compos == null)
			throw new MixtureException("Mixture.of: agruments can't be null");
		if(compList.size() != compos.getNumComp())
			throw new MixtureException("Mixture.of: size of component list and composition must be equal");
		for(Component c : compList)
			if(c == null)
				throw new MixtureException("Mixture.of: component list can't contain null elements");
		return new BaseMixture(compList, compos);
	}
	
	@Override
	public int getNumComp() {
		return components.size();
	}
	
	@Override
	public Composition getComposition() {
		return composition;
	}
	
	@Override
	public List<Component> getComponents() {
		return components;
	}

	// Interface definitions for mixture properties calculation
	
	public static interface MixDensityCalculator {
		public double calculate(Mixture mix, double temp) throws MixPropertyCalculationException;
	}
	
	public static interface MixViscosityCalculator {
		public double calculate(double temp);
	}
	
	public static interface MixEnthalpyOfVaporizationCalculator {
		public double calculate(double temp);
	}
	
	public static interface BoilingTemperatureCalculator {
		public double calculate(double pressure);
	}
	
	public static interface PressureCalculator {
		public double calculate(double temp);
	}
	
	public static interface MixHeatCapacityOfVaporCalculator {
		public double calculate(double temp);
	}
	
	public static interface MixHeatCapacityOfLiquidCalculator {
		public double calculate(double temp);
	}
	
	public static interface MixEnthalpyOfVaporCalculator {
		public double calculate(double temp);
	}
	
	public static interface MixEnthalpyOfLiquidCalculator {
		public double calculate(double temp);
	}
	
	private MixDensityCalculator mixDensityCalc;
	private MixViscosityCalculator mixViscosityCalc;
	
	
	@Override
	public double getMixDensity(double temp) throws MixPropertyCalculationException, MixPropertyCalculatorNotDefinedException {
		if(mixDensityCalc == null)
			throw new MixPropertyCalculatorNotDefinedException();
		return mixDensityCalc.calculate(this, temp);
	}

	@Override
	public double getMixViscosity(double temp) {
		return mixViscosityCalc.calculate(temp);
	}

	@Override
	public double getMixEnthalpyOfVap(double temp) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMixEnthalpyOfLiq(double temp) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMixEnthalpyOfVaporiz(double temp) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMixPressure(double temp) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMixHeatCapacityOfVap(double temp) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMixHeatCapacityOfLiq(double temp) {
		// TODO Auto-generated method stub
		return 0;
	}





}
