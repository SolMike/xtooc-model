package ru.qdts.xtooc.model.mixture;

import java.util.List;

import ru.qdts.xtooc.model.component.Component;

public interface Mixture {

    double getMixDensity(double temp) throws MixPropertyCalculationException, MixPropertyCalculatorNotDefinedException;
    double getMixViscosity(double temp);
    double getMixEnthalpyOfVap (double temp);
    double getMixEnthalpyOfLiq (double temp);
    double getMixEnthalpyOfVaporiz(double temp);
    double getMixPressure (double temp);
    double getMixHeatCapacityOfVap(double temp);
    double getMixHeatCapacityOfLiq(double temp);

    int getNumComp();

    Composition getComposition();

    List<Component> getComponents();
}
