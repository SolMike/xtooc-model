package ru.qdts.xtooc.model.component;

public class BaseComponent implements Component {

    protected final String name;
    protected final double molecularMass;
    protected final double tempCritical;
    protected final double stEnthalpy;
    protected final double stEnthalpyOfVaporiz;
    
	public static interface DensityCalculator {
		public double calculate(double temp);
	}
	
	public static interface ViscosityCalculator {
		public double calculate(double temp);
	}
	
	public static interface EnthalpyOfVaporizationCalculator {
		public double calculate(double temp);
	}
	
	public static interface BoilingTemperatureCalculator {
		public double calculate(double pressure);
	}
	
	public static interface PressureCalculator {
		public double calculate(double temp);
	}
	
	public static interface HeatCapacityOfVaporCalculator {
		public double calculate(double temp);
	}
	
	public static interface EnthalpyOfVaporCalculator {
		public double calculate(double temp);
	}
	
	public static interface EnthalpyOfLiquidCalculator {
		public double calculate(double temp);
	}
	
	private DensityCalculator densityCalc;
	private ViscosityCalculator viscosityCalc;
	private EnthalpyOfVaporizationCalculator enthOfVzationCalc;
	private BoilingTemperatureCalculator boilTempCalc;
	private PressureCalculator pressureCalc;
	private HeatCapacityOfVaporCalculator heatCapOfVCalc;
	private EnthalpyOfVaporCalculator enthOfVCalc;
	private EnthalpyOfLiquidCalculator enthOfLCalc;
	
	public BaseComponent(String name, double molecMass, double tempCritical, double stEnthalpy,
			double stEnthalpyOfVaporiz, DensityCalculator densityCalc, ViscosityCalculator viscosityCalc,
			EnthalpyOfVaporizationCalculator enthOfVzationCalc, BoilingTemperatureCalculator boilTempCalc,
			PressureCalculator pressureCalc, HeatCapacityOfVaporCalculator heatCapOfVCalc,
			EnthalpyOfVaporCalculator enthOfVCalc, EnthalpyOfLiquidCalculator enthOfLCalc) {
		super();
		this.name = name;
		this.molecularMass = molecMass;
		this.tempCritical = tempCritical;
		this.stEnthalpy = stEnthalpy;
		this.stEnthalpyOfVaporiz = stEnthalpyOfVaporiz;
		this.densityCalc = densityCalc;
		this.viscosityCalc = viscosityCalc;
		this.enthOfVzationCalc = enthOfVzationCalc;
		this.boilTempCalc = boilTempCalc;
		this.pressureCalc = pressureCalc;
		this.heatCapOfVCalc = heatCapOfVCalc;
		this.enthOfVCalc = enthOfVCalc;
		this.enthOfLCalc = enthOfLCalc;
	}

	@Override
	public double getDensity(double temp) throws PropertyCalculatorNotDefinedException {
		if(densityCalc == null)
			throw new PropertyCalculatorNotDefinedException("DENSITY");
		
		return densityCalc.calculate(temp);
	}

	@Override
	public double getViscosity(double temp) throws PropertyCalculatorNotDefinedException {
		if(densityCalc == null)
			throw new PropertyCalculatorNotDefinedException("VISCOSITY");
		
		return viscosityCalc.calculate(temp);
	}

	@Override
	public double getEnthalpyOfVaporiz(double temp) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getTempBoil(double pressure) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPressure(double temp) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getHeatCapacityOfVap(double temp) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getHeatCapacityOfLiq(double temp) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEnthalpyOfVap(double temp) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEnthalpyOfLiq(double temp) throws PropertyCalculatorNotDefinedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

//	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getMolecularMass() {
		return molecularMass;
	}

//	@Override
	public void setMolecMass(double molecMass) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getTempCritical() {
		return tempCritical;
	}

//	@Override
	public void setTempCritical(double tempCritical) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getStEnthalpy() {
		return stEnthalpy;
	}

//	@Override
	public void setStEnthalpy(double stEnthalpy) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getStEnthalpyOfVaporiz() {
		return stEnthalpyOfVaporiz;
	}

//	@Override
	public void setStEnthalpyOfVaporiz(double stEnthalpyOfVaporiz) {
		// TODO Auto-generated method stub

	}
	
	public class Builder {
		
	}

}
