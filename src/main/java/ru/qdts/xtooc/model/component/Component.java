package ru.qdts.xtooc.model.component;

public interface Component {

    String getName();
    double getMolecularMass();
    double getTempCritical();
    double getStEnthalpy();
    double getStEnthalpyOfVaporiz();
	
	double getDensity(double temp) throws PropertyCalculatorNotDefinedException;
    double getViscosity(double temp) throws PropertyCalculatorNotDefinedException;
    double getEnthalpyOfVaporiz(double temp) throws PropertyCalculatorNotDefinedException;
    double getTempBoil(double pressure) throws PropertyCalculatorNotDefinedException;
    double getPressure (double temp) throws PropertyCalculatorNotDefinedException;
    double getHeatCapacityOfVap(double temp) throws PropertyCalculatorNotDefinedException;
    double getHeatCapacityOfLiq(double temp) throws PropertyCalculatorNotDefinedException;
    double getEnthalpyOfVap(double temp) throws PropertyCalculatorNotDefinedException;
    double getEnthalpyOfLiq(double temp) throws PropertyCalculatorNotDefinedException;
}
