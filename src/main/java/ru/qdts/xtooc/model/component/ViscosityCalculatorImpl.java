package ru.qdts.xtooc.model.component;

import ru.qdts.xtooc.model.component.BaseComponent.ViscosityCalculator;

public class ViscosityCalculatorImpl implements ViscosityCalculator {

	private final double A;
	private final double B;
	
	public ViscosityCalculatorImpl(double a, double b) {
		super();
		A = a;
		B = b;
	}

	/** Метод для определения вязкости
     * η = 10^(A*((1/T)-(1/B)))
     * @param temp in K units
     * @return viscosity in cPs (сПз) units
     */
	@Override
	public double calculate(double temp) {
		return Math.pow(10,A *((1/temp) - (1/B)));
	}

}
